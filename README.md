# Shopping list

Backend is built with Laravel 9 and the Frontend is separated with Vue3 Composition API

## Backend

Clone the repo

Copy and rename the .env.example to .env

Update the DB-config to use sqlite or use your preferred database.

DB_CONNECTION=sqlite

DB_DATABASE=./storage/database.sqlite

From the root of ./backend/:

```
composer install
```

```
php artisan key:generate
```

```
php artisan migrate
# if the php sqlite module is missing, in Ubuntu install it with 'apt install php-sqlite3' 
```

```
php artisan serve
```

## Front end

Clone the repo

From the root of ./frontend/

```sh
npm install
```

```sh
npm run dev
```

It is configured to use the API http://127.0.0.1:8000
