/**
 * API functionality
 */

/**
 * URL to the backend
 */
const APIUrl = 'http://127.0.0.1:8000/api'

/**
 * Headers
 */
const headers = { 'Content-Type': 'application/json' }

/**
 * Fetch all shopping lists and all their items
 * 
 * @returns <Promise> shopping lists
 */
async function getShoppingLists() {
    return await fetch(`${APIUrl}/shopping_list`)
    .then(response => response.json())
    .then(data => data.data)
    .catch(error => { console.log(error); })
}

/**
 * Create a new Shoppig list
 * 
 * @param {String} name name of the new shopping list
 * @returns <Promise> the created shopping list
 */
async function createShoppingList(name) {
    return await fetch(`${APIUrl}/shopping_list`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({ name: name }),
    })
    .then(response => response.json())
    .then(data => data.data)
    .catch(error => { console.log(error); })
}

/**
 *  Create a new Shopping list item
 * 
 * @param {String} name 
 * @param {Integer} shopping_list_id 
 * @returns Promise the created shopping list item
 */
async function createShoppingItem(name, shopping_list_id) {
    return await fetch(`${APIUrl}/shopping_item`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({ 
            name: name,
            shopping_list_id: shopping_list_id,
        }),
    })
    .then(response => response.json())
    .then(data => data.data )
    .catch(error => { console.log(error); })
}

/**
 * Update the selected Shopping list item
 * 
 * @param {Integer} id 
 * @param {String} name 
 * @returns <Promise> the uppdated shopping list item
 */
async function updateShoppingItem(id, name) {
    return await fetch(`${APIUrl}/shopping_item/${id}`, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify({ 
            name: name,
        }),
    })
    .then(response => response.json())
    .then(data => data.data )
    .catch(error => { console.log(error); })
}

/**
 * Delete the selected shopping list item
 * @param {Integer} id 
 * @returns <Promise> success message
 */
async function deleteShoppingItem(id) {
    console.log(`${APIUrl}/shopping_item/${id}`)
    return await fetch(`${APIUrl}/shopping_item/${id}`, {
        method: 'DELETE',
    })
    .then(response => response.json())
    .then(data => data.data )
    .catch(error => { console.log(error); })
}

export {
    getShoppingLists,
    createShoppingList,
    createShoppingItem,
    updateShoppingItem,
    deleteShoppingItem
}