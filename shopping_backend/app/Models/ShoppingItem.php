<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShoppingItem extends Model
{
    use HasFactory;
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'shopping_list_id'];

    /**
     * Each Shopping item belongs to a Shopping list
     *
     * @return void 
     */
    public function shopping_list() : BelongsTo {
        return $this->belongsTo(ShoppingList::class);
    }
}
