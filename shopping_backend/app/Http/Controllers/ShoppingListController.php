<?php

namespace App\Http\Controllers;

use App\Models\ShoppingList;
use Illuminate\Http\Request;
use App\Http\Resources\ShoppingListResource;

class ShoppingListController extends Controller
{
    /**
     * Display a listing of all the Shopping lists and their items.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ShoppingListResource::collection(ShoppingList::all());
    }


    /**
     * Store a newly created Shopping list in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create the shopping list
        $shopping_list = ShoppingList::create(
            [
                'name' => $request->name,
            ],
        );

        // return the shopping list resource
        return new ShoppingListResource($shopping_list);
    }

    /**
     * Display the specified Shopping list.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return the shopping list resource
        return new ShoppingListResource(ShoppingList::findOrFail($id));
    }


    /**
     * Update the specified Shopping list in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Fetch the shopping list or response with an error
        $shopping_list = ShoppingList::findOrFail($id);
        // set the name
        $shopping_list->name = $request->name;
        // save it
        $shopping_list->save();

        // return the updated shopping list
        return new ShoppingListResource($shopping_list);
        
    }

    /**
     * Remove the specified Shopping list from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Check if it exists or return an error response
        $shopping_list = ShoppingList::findOrFail($id);

        // remove it
        $shopping_list->delete();
    }
}
