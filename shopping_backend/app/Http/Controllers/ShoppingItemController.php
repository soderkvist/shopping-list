<?php

namespace App\Http\Controllers;

use App\Models\ShoppingItem;
use App\Models\ShoppingList;
use Illuminate\Http\Request;
use App\Http\Resources\ShoppingItemResource;

class ShoppingItemController extends Controller
{
    /**
     * Display a listing of all the Shopping items.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ShoppingItemResource::collection(ShoppingItem::all());
    }


    /**
     * Store a newly created Shopping item in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if the Shopping list exists or return an error message
        $shopping_list = ShoppingList::findOrFail($request->shopping_list_id);

        // Create the Shopping item
        $shopping_item = ShoppingItem::create(
            [
                'name' => $request->name,
                'shopping_list_id' => $shopping_list->id,
            ],
        );

        // Return the resource
        return new ShoppingItemResource($shopping_item);
    }

    /**
     * Display the specified Shopping item.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return the specifci  resouce
        return new ShoppingItemResource(ShoppingItem::findOrFail($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shopping_item = ShoppingItem::findOrFail($id);

        $shopping_item->name = $request->name;

        $shopping_item->save();

        return new ShoppingItemResource($shopping_item);
    }

    /**
     * Remove the specified Shopping item from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check if it exists
        $shopping_item = ShoppingItem::findOrFail($id);

        // remove it
        $shopping_item->delete();

        // return a success message
        return response()->json([
            'message' => 'success'
        ]);
        
    }
}
