<?php

namespace App\Http\Resources;

use App\Http\Resources\ShoppingListResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShoppingItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'name' => $this->name,
            'shopping_list' => new ShoppingListItemResource($this->shopping_list),
        ];
    }
}
